import React from 'react';
import { ThemeProvider, createTheme } from '@mui/material';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Background from './Background';
import Marcus from './Marcus';

const theme = createTheme({});

function App() {
  return (
    <ThemeProvider theme={theme}>
      <Router>
        <Routes>
          <Route path="/background" element={<Background />} />
          <Route path="/marcus" element={<Marcus />} />
        </Routes>
      </Router>
    </ThemeProvider>
  );
}

export default App;