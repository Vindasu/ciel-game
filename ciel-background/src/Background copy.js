import React, { useRef, useEffect, useState } from 'react';
import { Box } from '@mui/material';

const drawCloud = (context, x, y, radius) => {
    context.beginPath();
    context.arc(x, y, radius, 0, Math.PI * 2, false);
    context.fillStyle = 'white';
    context.fill();
};

const Background = () => {
    const canvasRef = useRef(null);
    const [clouds, setClouds] = useState([]);
    const animationFrameId = useRef(null);

    const handleCanvasClick = (event) => {
        const rect = canvasRef.current.getBoundingClientRect();
        const x = event.clientX - rect.left;
        const y = event.clientY - rect.top;

        setClouds(clouds.map(cloud => {
            const cloudStartX = cloud.x;
            const cloudEndX = cloud.x + 40; // The width of the cloud
            const cloudStartY = cloud.y; // The top of the cloud
            const cloudEndY = cloud.y + 40; // The bottom of the cloud

            if (x >= cloudStartX && x <= cloudEndX && y >= cloudStartY && y <= cloudEndY) {
                return { ...cloud, visible: false }; // Hide the cloud when it's clicked
            } else {
                return cloud;
            }
        }));
    };

    const spawnCloud = () => {
        if (!canvasRef.current || clouds.length >= 10) return;
        const newCloud = {
            x: -40, // Set the x-coordinate to just outside the left edge of the canvas
            y: Math.random() * canvasRef.current.height,
            visible: true
        };
        setClouds(prevClouds => [...prevClouds, newCloud]);
    };

    useEffect(() => {
        const canvas = canvasRef.current;
        if (!canvas) return;
        const context = canvas.getContext('2d');

        const gameLoop = () => {
            // Clear the canvas
            context.clearRect(0, 0, canvas.width, canvas.height);

            // Draw each cloud
            clouds.forEach(cloud => {
                if (cloud.visible) {
                    drawCloud(context, cloud.x, cloud.y, 20);
                    drawCloud(context, cloud.x + 15, cloud.y, 20);
                    drawCloud(context, cloud.x + 8, cloud.y - 20, 20);
                }

                // Move the cloud to the right
                cloud.x = (cloud.x + 0.1) % (canvas.width + 40);
            });

            animationFrameId.current = requestAnimationFrame(gameLoop);
        };

        gameLoop();

        return () => {
            cancelAnimationFrame(animationFrameId.current);
        };
    }, [clouds]);

    useEffect(() => {
        const spawnIntervalId = setInterval(spawnCloud, 4600);

        return () => {
            clearInterval(spawnIntervalId);
        };
    }, []);

    return (
        <Box
            sx={{
                position: 'absolute',
                top: 0,
                left: 0,
                width: '100%',
                height: '100%',
                backgroundColor: 'lightblue',
                overflow: 'hidden',
            }}
        >
            <canvas ref={canvasRef} style={{ width: '100%', height: '100%' }} onClick={handleCanvasClick} />
        </Box>
    );
};

export default Background;